package ex2;

import org.junit.Test;
import runner.Points;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;

public class BatchStoreTest {

    @Test
    @Points(3)
    public void batchStoreStoresNumberBatches() {

        BatchStore store = getBatchStore();
                           // defined below

        assertThat(store.getSize(), is(3));
    }

    @Test
    @Points(6)
    public void returnsSizeOfLargestBatch() {

        BatchStore store = getBatchStore();

        assertThat(store.getSizeOfLargestBatch(), is(4));
    }

    @Test
    @Points(8)
    public void returnsBatchWithMaxSum() {

        BatchStore store = getBatchStore();

        String batchAsString = batchToString(store.getBatchWithMaxSum());

        assertThat(batchAsString, is("1.0, 2.0, 8.0"));
    }

    private BatchStore getBatchStore() {
        BatchStore store = new BatchStore();

        List<Integer> integers = List.of(1, 2, 3, 4);
        List<Double> doubles = List.of(1.0, 2.0, 8.0);
        List<Float> floats = List.of(1f, 2f, 3f);

        store.addBatch(integers);
        store.addBatch(doubles);
        store.addBatch(floats);

        return store;
    }

    private String batchToString(List<?> batch) {
        ArrayList<String> elements = new ArrayList<>();
        for (Object o : batch) {
            if (!Number.class.isAssignableFrom(o.getClass())) {
                fail("batch should consist of numbers");
            }

            elements.add(String.valueOf(o));
        }

        return String.join(", ", elements);
    }
}
