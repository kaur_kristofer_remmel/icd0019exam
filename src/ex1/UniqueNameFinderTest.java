package ex1;

import org.junit.Test;
import runner.Points;

import java.util.List;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UniqueNameFinderTest {

    @Test
    @Points(3)
    public void findsOutWhetherThereIsAPersonWithTheSameName() {
        UniqueNameFinder finder = new UniqueNameFinder();

        finder.addPerson(new Person("Alice", "Smith"));
        finder.addPerson(new Person("Alice", "Taylor"));

        assertTrue(finder.containsPersonWithTheSameName(new Person("Alice", "Smith")));
        assertTrue(finder.containsPersonWithTheSameName(new Person("Alice", "Taylor")));
        assertFalse(finder.containsPersonWithTheSameName(new Person("Alice", "Adams")));
    }

    @Test
    @Points(6)
    public void findsCountOfPersonsWithUniqueNames() {

        UniqueNameFinder finder = getUniqueNameFinder();

        var personsWithUniqueNames = finder.personsWithUniqueNames();

        assertThat(personsWithUniqueNames.size(), is(4));
    }

    @Test
    @Points(8)
    public void findsPersonsWithUniqueNames() {

        UniqueNameFinder finder = getUniqueNameFinder();

        var personsWithUniqueNames = finder.personsWithUniqueNames();

        assertThat(personsWithUniqueNames, containsInAnyOrder(
                new Person("Bob", "Adams"),
                new Person("Alice", "Smith"),
                new Person("David", "Adams"),
                new Person("Bob", "Smith")
        ));
    }

    public UniqueNameFinder getUniqueNameFinder() {
        UniqueNameFinder finder = new UniqueNameFinder();

        List<String> firstNames = List.of("Alice", "Bob", "Carol", "David");
        List<String> lastNames = List.of("Smith", "Taylor", "Adams", "Jones");

        Random rnd = new Random(0);
        for (int i = 0; i < 20; i++) {
            String firstName = firstNames.get(rnd.nextInt(firstNames.size()));
            String lastName = lastNames.get(rnd.nextInt(lastNames.size()));

            finder.addPerson(new Person(firstName, lastName));
        }

        return finder;
    }

}
